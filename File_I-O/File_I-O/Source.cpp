#include <iostream> // allows input and output
#include<fstream> // allows access to files
#include <string> // allows the use of strings

void Write(); // initializes the write function
void Read(); // initializes the read function

void main()
{
	std::string yesOrNo; // holds the users choice
	
	std::cout << "\t\t\t\t\t\t\t***** This is a C++ Definition Machine *****\n"; // introduces the project

	Read(); // calls the read function

	do // does the following code until a requirement is met
	{
		std::cout << "Do you have anything you want to remember? \n"; // asks a question
		std::cout << "Yes or No \n"; // shows choices
		std::getline(std::cin, yesOrNo); // gets the users input

		if (yesOrNo == "Yes") // gets if the user said yes
		{
			Write(); // calls the write function
		}
		else
		{
			std::cout << "Okay then that is all good bye!\n"; // tells the user that the program will end
		}
	}
	while (yesOrNo == "Yes"); // defines the requirements of the do 
	
}

void Write()
{
	std::string stuffToRemeber; // hols what the user wants to remember

	std::ofstream fileObject("Memory.txt"); // accesses a file to store information

	std::cout << "\n"; // creates space
	std::cout << "What would you like me to remember? \n"; // asks a question

	std::getline(std::cin, stuffToRemeber); // gets the users input
	
	try // attempts to run the following code
	{
		std::cout << "We are about to write " << stuffToRemeber << " to a file. \n"; // lets the user know whats happening
		
		if (fileObject.is_open()) // checks if the file is open
		{
			std::cout << "The file is open.\n"; // tells the user it is open
			fileObject << stuffToRemeber << "\n"; // stores the users input
			std::cout << "This file has been written to.\n"; // tells the user that the process is done
		}
		else
		{
			std::cout << "File not open cannot continue. \n"; // lets the user know that it failed to open
			throw; // errors
		}

		std::cout << "Trying to close file! \n"; // lets the user know whats happening
		fileObject.close(); // closes the file
		std::cout << "The file is now closed \n"; // lets the user know that the file is closed
	}
	catch (...) // if an error occurs the code here runs
	{
		std::cout << "You had to really break it to get here... \n"; // lets the user know that they should not be here
	}
}

void Read()
{
	int keyWords = 0; // a counter for the key words
	int reservedWords = 0; // a counter for reserved words
	bool keyToReserved = false; // a toggle for counting
	
	try // attempts to run the following code
	{
		std::fstream file; // allows the use of files
		std::string definition, DefinitionRef; // stores the file and its information
		
		std::ofstream definitionCopy("CPP_Enhanced_Reference.txt"); // gets the new file

		DefinitionRef = "C++ References.txt"; // gets the definitions

		file.open(DefinitionRef.c_str()); // opens the definitions file

		while (std::getline(file, definition)) // does the following code while the conditions are met
		{
			if (definition == "+") // if there is a + 
			{
				keyToReserved = true; // toggles keyToReserved to true
			}

			if (keyToReserved == false) // if keyToReserved is false
			{
				keyWords++; // increase key words by 1
			}
			else // if keyToReserved is true
			{
				reservedWords++; // increase reserved words by 1
			}
		}
		reservedWords--; // decreases reserved words by one due to a logical error
		
		std::cout << "\n"; // adds a space line
		std::cout << "C++ Key words defined = " << keyWords << "\n"; // tells the user how many key words there are
		std::cout << "C++ Reserved words defined = " << reservedWords << "\n"; // tells the user how many reserved words there are
		std::cout << "\n"; // adds a line space

		definitionCopy << "C++ Key words defined = " << keyWords << "\n\n"; // puts the number of keywords on the new file

		file.clear(); // clears the file
		file.close(); // closes the file
		file.open(DefinitionRef.c_str()); // re-opens the file
		
		while (std::getline(file, definition)) // does the following code while the conditions are met
		{
			std::cout << definition << "\n"; // outputs the definitions

			definitionCopy << definition << "\n"; // puts the definitions in the new file

			if (definition == "+") // if definition equals +
			{
				definitionCopy << "C++ Reserved words defined = " << reservedWords << "\n\n"; // tells the user how many reserved there are
			}
		}
	}
	catch (...) // if an error occurs the code here runs
	{
		std::cout << "You really broke something to get here... \n"; // lets the user know that they should not be here
	}
}